/**
 * JSX was created to make JavaScript representation of HTML-like. To understand the difference between JSX and HTML consider the following syntax:
 * 
 * React.createElement(tagName, properties/attributes, innerHTML);
 * 
 * Actual DOM:
 * <div class="ui-items">
 *   Hello, I am basic React Component
 *  </div>
 * 
 * ReactDOM:
 * React.createElement('div', {'className': 'ui-items'}, 'Hello, I am basic React Component');
 * 
 * Equivalent JSX:
 * <div className="ui-items">
 *   Hello, I am basic React Component
 *  </div>
 * 
* Actual DOM:
 * <div class="ui-items">
 *    <p>
 *        Hello, I am basic React Component
 *    </p>
 *  </div>
 * 
 * ReactDOM:
 * React.createElement('div', {'className': 'ui-items'}, 
 *    React.createElement('p', null, 'Hello, I am basic React Component')
 * );
 * 
 * Equivalent JSX:
 * <div className="ui-items">
 *    <p>
 *        Hello, I am basic React Component
 *    </p>
 *  </div>
 */

/**
 * There are two ways to declare React coomponents:
 * 1. An ES6 classes
 * 2. Function Component (Functional Programming)
 * 
 * Sample Class Components:
 * class HelloWorld extends React.Component {
 *    render() {
 *        return (
 *           <div>
 *               <h2>Hello World From React!</h2>
            </div>
 *         );
 *     }
 * }
 * 
 */
//function App() {
//   return (
//     <div>
//       <h2>Hello World From React!</h2>
//     </div>
//   );
// }

// export default App;
