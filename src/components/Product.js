import React from 'react';

/*
All the props can be access as 'props'

A child can read the 'props' but can't modify it.

In React, Parent owns the prop (ProductList owns the prop of the Product)

React favors "One way data flow" 

Data Change comes from the "Top" to "Bottom"

Event is owned by child but it needs to report this event to parent. Parent will work on it and let child know about the change to perform.

Event propogates upwards.

Any time we define our own custome component methods, we have to manually bind 'this' to the component.


*/

class Product extends React.Component{
    // constructor(props){
    //     super(props)

    //     this.handleUpVote = this.handleUpVote.bind(this)
    // }


    handleUpVote = () => {
        this.props.onUpVote(this.props.id)
    }

    render(){
        return(
            <div className='item'>
                <div className='image'>
                    <img src={this.props.productImageUrl} alt={this.props.title}/>
                </div>

                <div className='middle aligned content'>
                    <div className='header'>
                        <a onClick={this.handleUpVote}><i className='large caret up icon'></i></a>
                        {this.props.votes}
                    </div>
                    <div className='description'>
                        <a><h3>{this.props.title}</h3></a>
                        <p>{this.props.description}</p>
                    </div>

                    <div className='extra'>
                        <span>Submmited By:</span>
                        <img
                            className='ui avatar image'
                            src={this.props.sumitterUrl} alt={this.props.title}/>
                    </div>
                    
                </div>
                
            </div>
        );
    }
}

export default Product;