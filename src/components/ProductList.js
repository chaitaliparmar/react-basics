import React from 'react';
import Product from './Product';
import Seed from '../seed';

/*
States are immmutable and props are mutabale
'this.state' is private to the component class but we can update it.

Every React Component is rendered as function of its 'props' and 'state'. This rendering is
called Determimistic (Theory of Computational wala deterministic).


For all state modifications after the initial state, React provides components a method this.setState()

Always treat state as an immutable object. You cannot change it directly.
*/

class ProductList extends React.Component{

state = {
    products: [],
}

//     constructor(props){
//         super(props);
        
// //      this.state is predefined object
//         this.state = {
//             products: [], 
//         }

//         this.handleProductUpVote = this.handleProductUpVote.bind(this);
//     }

    componentDidMount(){
        // console.log("from componentDidMount");
        this.setState({products: Seed.products});
    }

    handleProductUpVote = (productId) => {

        
        const newProducts = this.state.products.map((product) => {
            if(product.id === productId)
            {
                // return Object.assign({},product,{votes: product.votes + 1});
                return {
                    ...product,
                    votes: product.votes + 1
                }
            }
            return product;
        });

        this.setState({products: newProducts});

        // products.forEach((product) => {
        //     if(product.id === productId )
        //     {
        //         product.votes += 1;
        //     }
        // });

        // The above code is worng because in this even if you create another array and try to change the new one.
        // the changes are reflected on the prior one as arrays in JS are refernces.
    }

    render(){
        const products = this.state.products.sort((p1, p2) => (p2.votes - p1.votes));
        const productComponents = products.map((product) => (
            <Product 
                    key={"product-" + product.id}
                    id={product.id}
                    title={product.title}
                    description={product.description}
                    url={product.url}
                    votes={product.votes}
                    sumitterUrl={product.submitterAvatarUrl}
                    productImageUrl={product.productImageUrl}
                    onUpVote={this.handleProductUpVote}
                />
        ));

        return(
            <>
            <h1 className='ui dividing centered header'>Product List</h1>
            <div className='ui unstackable items container'>
                {productComponents}
            </div>
            </>
        );
    }
}

export default ProductList;